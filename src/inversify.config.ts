import { Container } from "inversify";
import Calculator from "./Calculator";
import MesnilsCalculator from "./MesnilsCalculator";
import NimesCalculator from "./NimesCalculator";
import CalculatorMediator from "./CalculatorMediator";

const container = new Container( { autoBindInjectable: true });

container.bind<CalculatorMediator>("CalculatorMediator").to(CalculatorMediator);
container.bind<Calculator>("Calculator").to(MesnilsCalculator);
container.bind<Calculator>("Calculator").to(NimesCalculator);

export default container;
