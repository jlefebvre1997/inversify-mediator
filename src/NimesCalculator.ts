import Calculator from "./Calculator";
import {injectable} from "inversify";
import "reflect-metadata";

@injectable()
export default class NimesCalculator implements Calculator {
    calculate(): void {
        console.log('Nimes');
    }

    supports(context: string): boolean {
        return context === 'nimes';
    }
}
