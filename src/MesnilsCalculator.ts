import Calculator from "./Calculator";
import {injectable} from "inversify";
import "reflect-metadata";

@injectable()
export default class MesnilsCalculator implements Calculator {
    calculate() {
        console.log('Mesnils');
    }

    supports(context: string): boolean {
        return context === 'mesnils';
    }
}
