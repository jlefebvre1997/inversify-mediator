import { injectable, multiInject } from "inversify";
import "reflect-metadata";
import Calculator from "./Calculator";

@injectable()
export default class CalculatorMediator {
    @multiInject<Calculator>("Calculator") private _calculators: Calculator[];

    calculate(context: string): void {
        for (const calculator of this._calculators) {
            if(calculator.supports(context)) {
                calculator.calculate()
            }
        }
    }
}
