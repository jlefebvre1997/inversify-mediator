export default interface Calculator {
    supports(context: string): boolean;
    calculate(): void;
}
