import container from "./inversify.config";
import CalculatorMediator from "./CalculatorMediator";

let calculator = container.get<CalculatorMediator>("CalculatorMediator")
console.log('Context : Nimes');
calculator.calculate('nimes');
console.log('Context : Mesnils');
calculator.calculate('mesnils');
